#!/usr/bin/env python ref
'''
Load mjcf, run in a loop get image, run mpi
'''
import pybullet as p
import cv2
import numpy as np
from numpy import cos as c
from numpy import sin as s
import time
import argparse
from mpi4py import MPI

parser = argparse.ArgumentParser()
parser.add_argument('--render', default=False, action='store_true')
args = parser.parse_args()

if args.render and MPI.COMM_WORLD.Get_rank() == 0:
    p.connect(p.GUI)
else:
    p.connect(p.DIRECT)
p.setGravity(0,0,-9.8)
def transform(r, p, y, pos, point):
    '''
    Get the transform required for the on body camera.
    '''
    r*=-1
    trans_matrix = np.array([[c(p)*c(p), -s(y)*c(r) + c(y)*s(p)*s(r), s(y)*s(r) + c(y)*s(p)*c(r), pos[0]],
    				[s(y)*c(p), c(y)*c(r) + s(y)*s(p)*s(r), -c(y)*s(r) + s(y)*s(p)*c(r), pos[1]],
    				[-s(p), c(p)*s(r), c(p)*c(r), pos[2]],
    				[0, 0, 0, 1]])
    return np.dot(trans_matrix, np.array([point[0], point[1], point[2], 1]))

def get_image():
    camTargetPos = [0.,0.,0.]
    cameraUp = [0,0,1]
    cameraPos = [1,1,1]
    yaw = 40
    pitch = 10.0
    roll=0
    upAxisIndex = 2
    camDistance = 4
    # pixelWidth = 320
    # pixelHeight = 240
    pixelWidth = 32
    pixelHeight = 32
    nearPlane = 0.01
    farPlane = 1000
    lightDirection = [0,1,0]
    lightColor = [1,1,1]#optional argument
    fov = 60
    pos, orn = p.getBasePositionAndOrientation(Id)
    roll, pitch, yaw = p.getEulerFromQuaternion(orn)

    dx = 0.8
    dy = 0
    dz = -1
    cam_dx = 0.2
    cam_dy = 0
    cam_dz = -0.15

    p_x, p_y, p_z, _ = transform(roll, pitch, yaw, pos, [dx,dy,dz])
    cam_x, cam_y, cam_z, _ = transform(roll, pitch, yaw, pos, [cam_dx, cam_dy, cam_dz])

    viewMatrix = p.computeViewMatrix([cam_x,cam_y,cam_z], [p_x, p_y, p_z], cameraUp)

    aspect = pixelWidth / pixelHeight;
    # projectionMatrix = p.computeProjectionMatrixFOV(fov, aspect, nearPlane, farPlane)
    projectionMatrix = (0.7499999403953552, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, -1.0000200271606445, -1.0, 0.0, 0.0, -0.02000020071864128, 0.0)

    try:
        image = p.getCameraImage(pixelWidth, pixelHeight, viewMatrix, projectionMatrix, lightDirection,lightColor, renderer = p.ER_TINY_RENDERER)
        return image[2]

        # rgb = image[2][:,:,:3]/255.0
        # depth = np.expand_dims(image[3], axis=2)
        # im = np.concatenate((rgb,depth), axis=2)
        # re_im = rgb*255.0
        # return np.nan_to_num(im)
        # return np.zeros([pixelWidth, pixelHeight, 4])

    except Exception as e:
        print(e)
        return np.zeros([pixelWidth, pixelHeight, 4])

def insert_line():
    boxHalfLength = 10
    boxHalfWidth = 0.01
    boxHalfHeight = 0.05

    groundHalfLength = 2.5
    groundHalfWidth = 1
    groundHalfHeight = 0.1
    mass = 1
    visualShapeId = -1
    segmentStart = 0
    q = p.getQuaternionFromEuler([0,0,0])
    new = p.createCollisionShape(p.GEOM_BOX,halfExtents=[boxHalfLength,0.15,boxHalfHeight])
    b1 = p.createMultiBody(baseMass=0,baseCollisionShapeIndex = new, basePosition = [boxHalfLength-1,-0.16, -0.04], baseOrientation=q)
    b2 = p.createMultiBody(baseMass=0,baseCollisionShapeIndex = new,basePosition = [boxHalfLength-1,0.16, -0.04], baseOrientation=q)
    return b1, b2


objs = p.loadMJCF("./mybot.xml",flags = p.URDF_USE_SELF_COLLISION_EXCLUDE_PARENT)
Id = objs[1]

insert_line()

jdict = {}
for j in range( p.getNumJoints(Id) ):
    info = p.getJointInfo(Id, j)
    link_name = info[12].decode("ascii")
    if info[2] != p.JOINT_REVOLUTE: continue
    jname = info[1].decode("ascii")
    jdict[jname] = j
motor_names = ["right_hip_x", "right_hip_z", "right_hip_y", "right_knee"]
motor_names += ["left_hip_x", "left_hip_z", "left_hip_y", "left_knee"]
motor_names += ["right_ankle_x", "right_ankle_y"]
motor_names += ["left_ankle_x", "left_ankle_y"]

motors = [jdict[n] for n in motor_names]

# Disable motors to use torque control:
p.setJointMotorControlArray(Id, motors, controlMode=p.VELOCITY_CONTROL, forces=[0.] * len(motor_names))
comm = MPI.COMM_WORLD
t = 0
t1 = time.time()
im = get_image()
turn = 0
while True:
    # print(t, comm.Get_rank(), comm.Get_size())
    if t%(10000) == 0:
        global_im = np.zeros_like(im)
        comm.Allreduce(im, global_im, op=MPI.SUM)
        if comm.Get_rank() == 0:
            print("t:", time.time() - t1, t*comm.Get_size())
        t1 = time.time()
    t += 1
    actions = np.random.random(12)*20
    p.setJointMotorControlArray(Id, motors,controlMode=p.TORQUE_CONTROL, forces=actions)
    im = get_image()
    p.stepSimulation()
    if args.render:
        cv2.imshow('frame', im)
        cv2.waitKey(1)
        time.sleep(0.02)

    pos, orn = p.getBasePositionAndOrientation(Id)
    if pos[2] < 0.75:
        p.resetBasePositionAndOrientation(Id, [0,0,1], [0,0,0,1])
        for m in motors:
            p.resetJointState(Id, m, 0.0)
